package org.elu.learn.spring.persistencejava.dao;

import org.elu.learn.spring.persistencejava.entities.Officer;
import org.elu.learn.spring.persistencejava.entities.Rank;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@Transactional
class JpaOfficeDAOTest {
    @Autowired
    private JpaOfficeDAO dao;
    @Autowired
    private JdbcTemplate template;

    // private method to retrieve the current ids in the database
    private List<Integer> getIds() {
        return template.query("select id from officers", (rs, num) -> rs.getInt("id"));
    }

    @Test
    void save() {
        var officer = new Officer(Rank.LIEUTENANT, "Wesley", "Crusher");
        assertThat(officer.getId()).isNull();

        officer = dao.save(officer);

        assertThat(officer.getId()).isNotNull();
    }

    @Test @Transactional(readOnly = true)
    void findOneThatExists() {
        getIds().forEach(id -> {
            var officer = dao.findById(id);

            assertThat(officer).isPresent();
            assertThat(officer.get().getId()).isEqualTo(id);
        });
    }

    @Test
    void findOneThatDoesNotExist() {
        var officer = dao.findById(9999);

        assertThat(officer).isEmpty();
    }

    @Test
    void findAll() {
        var dbNames = dao.findAll().stream()
                .map(Officer::getLastName)
                .toList();

        assertThat(dbNames).containsExactlyInAnyOrder("Kirk", "Picard", "Sisko", "Janeway", "Archer");
    }

    @Test
    void count() {
        assertThat(dao.count()).isEqualTo(5);
    }

    @Test
    void delete() {
        getIds().forEach(id -> {
            var officer = dao.findById(id);
            assertThat(officer).isPresent();
            dao.delete(officer.get());
        });
        assertThat(dao.count()).isEqualTo(0);
    }

    @Test
    void existsById() {
        getIds().forEach(id ->
                assertThat(dao.existsById(id)).isTrue());
    }
}
