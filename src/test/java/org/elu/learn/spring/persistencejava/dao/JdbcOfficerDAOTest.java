package org.elu.learn.spring.persistencejava.dao;

import org.elu.learn.spring.persistencejava.entities.Officer;
import org.elu.learn.spring.persistencejava.entities.Rank;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@Transactional
class JdbcOfficerDAOTest {
    @Autowired
    private JdbcOfficerDAO dao;

    @Test
    void save() {
        var officer = new Officer(Rank.ENSIGN, "Wesley", "Crusher");

        officer = dao.save(officer);

        assertThat(officer.getId()).isNotNull();
    }

    @Test
    void findByIdThatExists() {
        final var officer = dao.findById(1);

        assertThat(officer).isPresent();
        assertThat(officer.get().getId()).isEqualTo(1);
    }

    @Test
    void findByIdThatDoesNotExist() {
        final var officer = dao.findById(999);

        assertThat(officer).isEmpty();
    }

    @Test
    void count() {
        assertThat(dao.count()).isEqualTo(5);
    }

    @Test
    void findAll() {
        final var dbNames = dao.findAll().stream()
                .map(Officer::getLastName)
                .toList();

        assertThat(dbNames)
                .containsExactlyInAnyOrder("Kirk", "Picard", "Sisko", "Janeway", "Archer");
    }

    @Test
    void delete() {
        IntStream.rangeClosed(1, 5)
                .forEach(id -> {
                    var officer = dao.findById(id);
                    assertThat(officer).isPresent();
                    dao.delete(officer.get());
                });
        assertThat(dao.count()).isEqualTo(0);
    }

    @Test
    void existsById() {
        IntStream.rangeClosed(1, 5)
                .forEach(id -> assertThat(dao.existsById(id)).isTrue());
    }
}
