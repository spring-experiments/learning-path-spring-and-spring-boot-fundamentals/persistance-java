package org.elu.learn.spring.persistencejava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PersistenceJavaApplication {

    public static void main(String[] args) {
        SpringApplication.run(PersistenceJavaApplication.class, args);
    }

}
