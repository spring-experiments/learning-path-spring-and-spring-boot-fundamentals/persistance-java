package org.elu.learn.spring.persistencejava.dao;

import org.elu.learn.spring.persistencejava.entities.Officer;
import org.elu.learn.spring.persistencejava.entities.Rank;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OfficerRepository extends JpaRepository<Officer, Integer> {
    List<Officer> findAllByRankAndLastNameContaining(Rank rank, String string);
}
